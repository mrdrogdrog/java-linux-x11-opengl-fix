#!/bin/bash
rm libxx_32.so libxx_64.so
g++ -o libxx_32.so -shared -fPIC -Wl,-soname,libxx.so -L/usr/lib/X11 -I/usr/include/X11 xdll.cpp -lX11 -m32
g++ -o libxx_64.so -shared -fPIC -Wl,-soname,libxx.so -L/usr/lib/X11 -I/usr/include/X11 xdll.cpp -lX11 -m64
